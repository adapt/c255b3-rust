use curve25519_dalek::{EdwardsPoint, Scalar};

/// A signature domain.
///
/// A [Signature][crate::Signature] is only valid under a particular domain.
///
/// The basic idea here is being able to use the same secret key in multiple contexts,
/// without the signature produced in one context being valid in another.
///
/// E.g. if you are signing values to attest the state of some data when you received it,
/// you don't want that to be able to be used as an endorsement of that data.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct Domain(pub &'static [u8; 32]);

const DEF: Domain = Domain(&[0; 32]);

impl Default for Domain {
    fn default() -> Self {
        DEF
    }
}

impl AsRef<[u8; 32]> for Domain {
    fn as_ref(&self) -> &[u8; 32] {
        &self.0
    }
}

impl Domain {
    /// Hash the curve point and the message together with this domain.
    pub(crate) fn hash_to_scalar(&self, r: &EdwardsPoint, msg: &[u8]) -> Scalar {
        Scalar::from_bytes_mod_order(self.hash(r.compress().as_bytes(), msg))
    }

    /// Hash a prefix and a message together with Blake3 keyed to this domain.
    pub(crate) fn hash(&self, prefix: &[u8; 32], msg: &[u8]) -> [u8; 32] {
        *blake3::Hasher::new_keyed(&self.0)
            .update(prefix)
            .update(msg)
            .finalize()
            .as_bytes()
    }
}
