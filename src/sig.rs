use curve25519_dalek::Scalar;

use crate::Error;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
/// A c255b3 signature.
pub struct Signature {
    pub(crate) s: Scalar,
    pub(crate) e: Scalar,
}

impl Signature {
    /// Render the signature into a byte array for external use.
    pub fn to_bits(&self) -> [u8; 64] {
        let mut bytes = [0u8; 64];
        let (eb, sb) = bytes.split_at_mut(32);
        eb.copy_from_slice(self.e.as_bytes());
        sb.copy_from_slice(self.s.as_bytes());
        bytes
    }

    /// Parse a signature from a byte array, verifying its format.
    pub fn parse_bits(value: &[u8; 64]) -> Result<Signature, Error> {
        let (rb, sb) = value.split_at(32);
        let e = Scalar::from_bits(rb.try_into().unwrap());
        let s = Scalar::from_bits(sb.try_into().unwrap());
        // variable time is okay during parsing
        if e.is_canonical().into() && s.is_canonical().into() {
            Ok(Signature { e, s })
        } else {
            Err(Error::NonCanonicalSignature)
        }
    }
}
