use crate::{Domain, Error, PublicKey, SecretKey, Signature};

/// A c25519 keypair, to sign or verify messages.
///
/// There's no particular advantage to using this instead of [SecretKey] and [PublicKey] seperately,
/// but it may be convenient to keep track of them as a single unit.
#[derive(Clone, Debug)]
pub struct Keys {
    /// The secret key.
    pub sk: SecretKey,
    /// The public key.
    pub pk: PublicKey,
}

impl Keys {
    /// Verify a message in a particular domain.
    pub fn verify_domain(&self, domain: Domain, sig: &Signature, msg: &[u8]) -> Result<(), Error> {
        self.pk.verify_domain(domain, sig, msg)
    }

    /// Sign a message for a particular domain, with a derived nonce.
    pub fn sign_domain(&self, domain: Domain, msg: &[u8]) -> Signature {
        self.sk.sign_domain(domain, msg)
    }

    /// Sign a message for the default domain.
    pub fn sign(&self, msg: &[u8]) -> Signature {
        self.sign_domain(Domain::default(), msg)
    }

    /// Verify a message in the default domain.
    pub fn verify(&self, sig: &Signature, msg: &[u8]) -> Result<(), Error> {
        self.pk.verify_domain(Domain::default(), sig, msg)
    }

    /// Sign a message for a particular domain, with the supplied secret nonce.
    ///
    /// If you really want randomized signatures, this is the function to use.
    /// Make sure you have a really solid random number generator making these nonces!
    pub fn sign_nonce(&self, domain: Domain, nonce: &[u8; 32], msg: &[u8]) -> Signature {
        self.sk.sign_nonce(domain, nonce, msg)
    }
}
